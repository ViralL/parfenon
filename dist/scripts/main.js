'use strict';

// ready
$(document).ready(function () {

    // anchor
    $(".anchor").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top - 50 }, 1000);
    });
    // anchor

    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $('.page-header__menu').addClass('fixed');
        $('body').css('overflow', 'hidden');
        return false;
    });
    $('.main-nav__close--js').click(function () {
        $('.page-header__menu').removeClass('fixed');
        $('body').css('overflow', 'visible');
        return false;
    });
    // adaptive menu

    // mask phone {maskedinput}
    // $("[name=phone]").mask("+7(999) 999-99-99");
    $("[name=date]").mask("99:99");
    // mask phone

    // slider {slick-carousel}
    var sliderMain = $('.slick-slider');
    var sliderCatalog = $('.slick-slider-cat');
    var sliderService = $('.carousel');
    var carouselHistory = $('.history-carousel');
    var sliderPopup = $('.slider-popup');
    var statusA = $('.slider-num');
    sliderService.slick({
        prevArrow: $('.slider-arrow-left'),
        nextArrow: $('.slider-arrow-right')
    });
    carouselHistory.slick({
        infinite: true,
        slidesToShow: 4,
        prevArrow: $('.slider-arrow-left'),
        nextArrow: $('.slider-arrow-right'),
        responsive: [{
            breakpoint: 1426,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 630,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    sliderMain.slick({
        dots: true,
        appendDots: $('.slider-dotsm'),
        arrows: false,
        // autoplay: true,
        draggable: false,
        speed: 0
    });
    sliderCatalog.slick({
        prevArrow: $('.slider-arrow-left'),
        nextArrow: $('.slider-arrow-right'),
        appendDots: $('.slider-dots'),
        dots: true
    });
    sliderMain.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        $('.slider-inner__title').removeClass('animated slideDown');
        $('.slider-inner__forwhat').removeClass('animated slideUp');
        $('.slider-more').removeClass('animated slideUp');
        $('.slider-inner__info').removeClass('animated fadeIn');
    });
    sliderMain.on('afterChange', function (event, slick, currentSlide, nextSlide) {
        $('.slider-inner__title').addClass('animated slideDown');
        $('.slider-inner__forwhat').addClass('animated slideUp');
        $('.slider-more').addClass('animated slideUp');
        $('.slider-inner__info').addClass('animated fadeIn');
    });
    function initSliders() {
        sliderPopup.slick({
            prevArrow: $('.slider-arrow-left'),
            nextArrow: $('.slider-arrow-right'),
            slidesToShow: 4,
            slidesToScroll: 1,
            variableWidth: true,
            centerMode: true,
            adaptiveHeight: true,
            //  centerPadding: '5px',
            responsive: [{
                breakpoint: 999,
                settings: {
                    slidesToShow: 1,
                    centerMode: false,
                    variableWidth: false
                }
            }]
        });
        sliderPopup.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
            var i = (currentSlide ? currentSlide : 0) + 1;
            statusA.html('<span class="active">' + i + '</span>' + '/' + slick.slideCount);
        });
    }
    // slider

    // select {select2}
    // $('select').select2({});
    // select

    // popup {magnific-popup}
    $('.popup').magnificPopup({
        showCloseBtn: false,
        removalDelay: 500, //delay removal by X to allow out-animation
        callbacks: {
            open: initSliders,
            close: function close() {
                sliderPopup.slick('unslick');
            }
        },
        midClick: true
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
    // $('.popup-gallery').magnificPopup({
    //     delegate: 'a',
    //     type: 'image',
    //     tLoading: 'Loading image #%curr%...',
    //     mainClass: 'mfp-with-zoom mfp-img-mobile',
    // 	image: {
    // 		verticalFit: true,
    // 		// titleSrc: function(item) {
    // 		// 	return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
    // 		// }
    // 	},
    // 	gallery: {
    // 		enabled: true
    // 	},
    // 	zoom: {
    // 		enabled: true,
    // 		duration: 300, // don't foget to change the duration also in CSS
    // 		opener: function(element) {
    // 			return element.find('img');
    // 		}
    // 	}
    // });
    // popup

    //search
    $('.search--js').click(function () {
        $('.search-body').addClass('active');
    });
    $('.search-close--js').click(function () {
        $('.search-body').removeClass('active');
    });
    //search

    //fullpage
    $('#fullpage').fullpage({
        hybrid: true,
        fitToSection: false,
        anchors: ['firstPage', 'secondPage', '3rdPage', '4rdPage'],
        navigation: true,
        navigationPosition: 'right'
    });
    //fullpage

    //tabs
    $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
        $(this).addClass('active').siblings().removeClass('active').closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    //tabs

    //type file
    $('.input__file-js').change(function () {
        $('.input__file-js').each(function () {
            var name = this.value;
            var reWin = /.*\\(.*)/;
            var fileTitle = name.replace(reWin, "$1");
            var reUnix = /.*\/(.*)/;
            fileTitle = fileTitle.replace(reUnix, "$1");
            $('.input__text-js .txt').text('Загружено: ' + fileTitle);
        });
    });
    //type file
});
// ready

// load
$(document).load(function () {});
// load

// scroll
$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('header').addClass("sticky");
    } else {
        $('header').removeClass("sticky");
    }
});
// scroll

function initMap() {
    var mapOptions = {
        center: new google.maps.LatLng(59.91916157, 30.3251195),
        zoom: 16,
        mapTypeControl: false,
        zoomControl: false,
        scrollwheel: false,
        styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
    };
    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(59.91916157, 30.3251195),
        map: map,
        icon: "../images/bubble.png"
    });
}

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {}
// mobile sctipts
//# sourceMappingURL=main.js.map
